<?php

$page = "index";
$term = "";

$articles = array(
  "General Election I" => "election",
  "General Election II" => "election",
  "The July 10th Crisis" => "article",
  "Betty Boothroyd" => "article",
  "22 June Terrorist Attack" => "article"
);

$bettyPages = json_decode(file_get_contents("http://localhost:14000/api/xombp/pages"));
foreach($bettyPages as $article => $type)
	$articles[$article] = $type;

if(isset($_GET["search"])){
  $term = $_GET["search"];
  if(isset($articles[$term]))
    $page = $articles[$term];
  else
    $page = "character";
}
else if (isset($_GET["char"])){
  $page = "character";
  $term = $_GET["char"];
}

function printTitle($title) {
  global $term;
  echo '<h1 style="margin-bottom: 0.25em;">' . $title . '</h1>';
  echo '<span style="font-size: 92%">From Betty, the free XOMBP encyclopedia<br />';
  if ($term != $title) echo "Redirected from ", $term, "<br/></span>";
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>XOMBPedia</title>
    <link href="/wiki.css" rel="stylesheet">
    <meta charset="utf-8"> <meta charset="UTF-8">
    <meta name="description" content="Free XOMBP encyclopedia">
    <meta name="keywords" content="XOBMP, Model, British, Parliament, UK">
    <meta name="author" content="Preben Vangberg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <header>
      <h1><a href="/">XOMBPedia</a></h1>
      <form>
        <input type="search" list="terms" name="search"/>
        <input type="submit">
        <datalist id="terms">
          <?php foreach($articles as $name => $type) echo '<option value="', $name, '">' ?>
        </datalist>
      </form>
    </header>
    <nav>
      <a href="/">
        <img class="logo" src="/xombp.jpg">
        <h3>XOMBPedia</h3>
      </a>
      <ul type="none">
        <li><a href="https://xombp.neocities.org">Main page</a></li>
        <li><a href="">Join now</a></li>
        <li><a href="https://xombp.neocities.org/legislation">Legislative archive</a></li>
        <li><a href="https://docs.google.com/spreadsheets/d/1IQxNJkf9ECDAOIsUUNGnZXAMseLtOsbRzqg7RIHNfcU/edit#gid=1354228655">Offical spreadsheet</a></li>
        <li><a href="https://gitlab.com/prvInSpace/xombpedia">Source code</a></li>
      </ul>
    </nav>
    <main>
      <?php require("pages/" . $page . ".php"); ?>
    </main>
  </body>
</html
