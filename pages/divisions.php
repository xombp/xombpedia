<?php

if(!isset($_GET["id"])): ?>
<section class='dark'>
  <h1>Missing Parameter</h1>
  <p>Parameter 'id' was not provided</p>
</section>
<?php
return;
endif;

$id = $_GET["id"];
$obj = json_decode(file_get_contents("bills/single.json"));

function filterVotes($votes, $needle){
  return array_filter($votes, function ($value) use ($needle) {
    return $value->vote == $needle; 
  });
}

function printVoteType($votes, $needle, $header){
  $filtered = filterVotes($votes, $needle);
  if(sizeof($filtered) > 0 ){
    echo "<h4>", $header, " (", sizeof($filtered), ")", "</h4>";
    echo '<ul type="none">';
    foreach($filtered as $char)
      echo "<li>", '<img class="party-icon" src="', $char->logo, '"> ', $char->fullName, "</li>";
    echo "</ul>";
  }
}

function printDivision($votes){
  printVoteType($votes, "AYE", "The ayes to the right");
  printVoteType($votes, "NO", "The noes to the left");
  printVoteType($votes, "CON", "The contents");
  printVoteType($votes, "NOT", "The not contents");
  printVoteType($votes, "PRE", "Presents");
  printVoteType($votes, "ABS", "Abstentions");
}

?>
<section class="dark">
  <h1><?=strtoupper($id)?>: Divisions</h1>
  <span class="centre"><a href="https://xombp.neocities.org/legislation/<?=strtolower($id)?>.html">Main page</a></span>
</section>
<?php
foreach($obj->results as $division){
  echo "<details>";
  echo "<summary class='", strtolower($division->house), "'>",
    $division->date, ": ",
    $division->typeHeader, "</summary><section>";
  if(sizeof((Array) $division->votes) == 1){
    printDivision(current((Array)$division->votes));
  }
  else {
    echo "Size != 1";
  }
  echo "</section></details>";
}
?>