<?php

$url = "http://localhost:14000/api/xombp/election/" . urlencode($term);
$data = json_decode(file_get_contents($url))->results[0];

$totalVotes = array_sum(array_map(function ($a) {
  return $a->votes;
}, $data->national));

function printParties()
{
  global $data;
  global $totalVotes;
  usort($data->national, function($a, $b){ return $b->votes - $a->votes; });
  $rows = array_chunk($data->national, 2);
  foreach ($rows as $row) {
    echo "<tr><td></td>";
    foreach ($row as $party) {
      echo '<td style="text-align:center;border-bottom: 6px solid ', $party->color, '">';
      echo '<img class="leader" src="';
      if (isset($party->leader->facesteal))
        echo $party->leader->facesteal;
      else
        echo "http://www.newdesignfile.com/postpic/2014/07/generic-user-icon-windows_352871.png";
      echo '">';
    }
    echo '</tr><tr><th style="text-align: left;">Leader</th>';
    foreach ($row as $party) {
      echo '<td><a href="?char=', $party->leader->name, '">', $party->leader->name, "</a></td>";
    }
    echo '</tr><tr><th style="text-align: left;">Party</th>';
    foreach ($row as $party) {
      echo "<td>", $party->name, "</td>";
    }
    echo '</tr><tr><th style="text-align: left;">Seats won</th>';
    foreach ($row as $party) {
      echo "<td>", $party->seats, "</td>";
    }
    echo "</tr><tr><th>Popular vote</th>";
    foreach ($row as $party) {
      echo "<td>", number_format($party->votes), "</td>";
    }
    echo '</tr><tr><th style="text-align: left;">Percentage</th>';
    foreach ($row as $party) {
      echo "<td>", round(100 * $party->votes / $totalVotes, 1), "%</td>";
    }
  }
}


?>

<?php printTitle($term); ?>

<table class="infobox">
  <caption><?= $term ?></caption>
  <tbody>
    <tr>
      <td colspan="2">
        <img class="flag" src="https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/50px-Flag_of_the_United_Kingdom.svg.png">
        <hr />
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table style="width: 330px; table-layout: fixed; margin-right: auto; margin-left: auto;">
          <tr>
            <td style="text-align: left;"><?php if (isset($data->previousElection)) echo '<a href="?search=', $data->previousElection, '">Previous</a>' ?></td>
            <td style="text-align: centre;"><?= (new DateTime($data->date))->format("j F Y"); ?></td>
            <td style="text-align: right;"><?php if (isset($data->nextElection)) echo '<a href="?search=', $data->nextElection, '">Next</a>' ?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
	  <td colspan="2">
		<hr style="margin-bottom: 0.7em;"/>
		<?= $data->seats ?> seats in the House of Commons
      </td>
    </tr>
    <tr>
      <td colspan="2"><?= ceil($data->seats / 2) ?> seats needed for a Majority</td>
    </tr>
    <!-- List of Parties -->
    <tr>
      <td colspan="2">
        <table>
          <tr>
            <th style="text-align: left;">Registered</th>
            <td colspan="2"><?= number_format($data->registeredVoters) ?></td>
          </tr>
          <tr>
            <th style="text-align: left;">Turnout</th>
            <td colspan="2"><?= round(100 * $totalVotes / $data->registeredVoters, 1) ?>%</td>
          </tr>
          <?php printParties(); ?>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<?php require("elections/" . $term . ".php"); ?>
