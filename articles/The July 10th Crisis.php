
<table width=320px class="infobox">
  <caption><?= $term ?></caption>
  <tbody>
    <tr>
      <td colspan="2">
        <img src="https://www.thetimes.co.uk/imageserver/image/%2Fmethode%2Ftimes%2Fprod%2Fweb%2Fbin%2Fd01d2c2a-bf8d-11ea-bb37-3d3cce807650.jpg?crop=3462%2C1947%2C19%2C104&resize=500">  
      <td>
    </tr>
    <tr>
      <td colspan="2">PM Stephen Percy arrives at 10 Downing Street<hr/></td>
    </tr>
    <tr style="text-align: left">
      <th>Date</th>
      <td>July 10, 2020</td>
    </tr>
    <tr style="text-align: left">
      <th>Location</th>
      <td>London, United Kingdom</td>
    </tr>
    <tr style="text-align: left; vertical-align: top;">
      <th style="padding-top: 1.05em;">Result</th>
      <td>
        <ul style="padding-left: 0.3em;">
          <li>Resignation of the Prime Minister, Stephen Percy and the fall of his Government.</li>
          <li>Arthur Haigh was invited by the Her Majesty the Queen to form a Government. This resulted in a coalition Government consisting of Labour, Social Democratic Party, Scottish and Northumbrian Independence Party and Plaid Cymru.</li>
          <li>The passage of the Prorogation (Reform) Act 2020, preventing future Prime Ministers from proroging Parliament without its explicit consent.</li> 
        </ul>
      </td>
    </tr>
  </tbody>
</table>

<p>
The <strong>10th of July crisis</strong> was a constitutional crisis that place on the 10th of July 2020 as a consequence of the <a href="?search=July+2020+By-Election">2020 by-election</a>.
</p>

<h2>Background</h2>

<h2>Events</h2>
<h3>Prorogation</h3>
<h3>House of Commons session</h3>
<h3>Sir Nicholas Clack speeding incident</h3>
<h3>March on Buckingham Palace</h3>
<h3>Stephen Percy's resignation</h3>

<h2>Aftermath</h2>