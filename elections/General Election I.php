<p>
The <strong>first 2019 United Kingdom general election</strong> was held on Sunday, 24 March 2019. It resulted in a Liberal Democrat, Labour, and National Autonomist Party coalition
with <a href="?char=Nicholas Clack">Sir Nicholas Clack</a> as the Prime Minister.
</p>

<h2>Aftermath</h2>

After the results were announced the Liberal Democrats, Labour, and National Autonomist Party